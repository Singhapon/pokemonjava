public class Arceus extends Pokemon{
    public Arceus(String name){
        super(name, 4000 ,120,"God",60000,100000,60005000,100000,6000,3);
    }
    public void attack(Pokemon enemy){
        System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(160);
       
    }


    public String skillName1(){
        return "Water Gun";
    }
    public String skillName2(){
        return "Bide";
    }
    public String skillName3(){
        return "Hydro Pump";
    }


    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(90);
        System.out.println(name + " HP: " + hp + " use skill Water Gun");
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(130);
        System.out.println(name + " HP: " + hp + " use skill Bide");
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(150);
        System.out.println(name + " HP: " + hp + " use Hydro Pump" );
    }


    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Fire")){
            return damage + 20;
        }
        else if(enemy.gettype().equals("Grass")){
            return damage - 20;
        }
        else 
            return damage;
    }
}