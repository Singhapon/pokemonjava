public class Torchic extends Pokemon{
    public Torchic(String name){
        super(name, 180 ,60,"Fire",25,20,70,500,100,2);
    }
    public void attack(Pokemon enemy){
        System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(60);
       
    }
    public String skillName1(){
        return "Ember";
    }
    public String skillName2(){
        return "Flame Charge";
    }
    public String skillName3(){
        return "Flamethrower";
    }
    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(80);
        System.out.println(name + " HP: " + hp + " use skill Ember" );
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(100);
        System.out.println(name + " HP: " + hp + " use skill Flame Charge" );
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(120);
        System.out.println(name + " HP: " + hp + " use skill Flamethrower" );
    }
    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Grass")){
            return damage + 20;
        }
        else if(enemy.gettype().equals("Water")){
            return damage - 20;
        }
        else 
            return damage;
    }
}