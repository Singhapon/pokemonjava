import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
public class pokemonGui extends JFrame{
    private Trainer trainer;
    private Pokemon pokemons;
    private int num;
    public pokemonGui(Trainer trainer){
        
        
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("mainPokemon.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        //back bar
        
        JLabel show = new JLabel();
        JButton back = new JButton();
        back.setBounds(10,10,70,40);
        background.add(back);
        back.setIcon(new ImageIcon("back.png"));

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                menuBar mg = new menuBar(trainer);
                setVisible(false);
            }
        });

       

        int y = 100;
        int i = 0;
        JButton name[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
        JButton change[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
        for(Pokemon p:trainer.getBag()){
        name[i].setText(" " + p.getName() + " HP: " + p.getHp() + " / " + p.maxHp());
        change[i].setIcon(new ImageIcon("changename.png"));
        change[i].setBounds(1160, y , 90 , 54);
        name[i].setBounds(800 , y , 362 , 54);
        y+=100;
        background.add(name[i]);
        background.add(change[i]);
        i++;
        } 
        change[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                String name = JOptionPane.showInputDialog(null,"Change your Pokemon name : ","Enter name here");
                trainer.getBag().get(0).changename(name);
                pokemonGui xd = new pokemonGui(trainer);
                setVisible(false);
                // name[0].setText(" " + trainer.getBag().get(0).getName() + "HP: " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
            }
        });
        change[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                String name = JOptionPane.showInputDialog(null,"Change your Pokemon name : ","Enter name here");
                trainer.getBag().get(1).changename(name);
                pokemonGui xd = new pokemonGui(trainer);
                setVisible(false);
                // name[0].setText(" " + trainer.getBag().get(0).getName() + "HP: " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
            }
        });
        change[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                String name = JOptionPane.showInputDialog(null,"Change your Pokemon name : ","Enter name here");
                trainer.getBag().get(2).changename(name);
                pokemonGui xd = new pokemonGui(trainer);
                setVisible(false);
                // name[0].setText(" " + trainer.getBag().get(0).getName() + "HP: " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
            }
        });
        change[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                String name = JOptionPane.showInputDialog(null,"Change your Pokemon name : ","Enter name here");
                trainer.getBag().get(3).changename(name);
                pokemonGui xd = new pokemonGui(trainer);
                setVisible(false);
                // name[0].setText(" " + trainer.getBag().get(0).getName() + "HP: " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
            }
        });
        change[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                String name = JOptionPane.showInputDialog(null,"Change your Pokemon name : ","Enter name here");
                trainer.getBag().get(4).changename(name);
                pokemonGui xd = new pokemonGui(trainer);
                setVisible(false);
                // name[0].setText(" " + trainer.getBag().get(0).getName() + "HP: " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
            }
        });
        change[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                String name = JOptionPane.showInputDialog(null,"Change your Pokemon name : ","Enter name here");
                trainer.getBag().get(5).changename(name);
                pokemonGui xd = new pokemonGui(trainer);
                setVisible(false);
                // name[0].setText(" " + trainer.getBag().get(0).getName() + "HP: " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
            }
        });
        show.setBounds(110,100,600,600);
        background.add(show);
        name[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                // mudkip.setVisible(false);
                // torchic.setVisible(false);
                // arceus.setVisible(false);
                // palkia.setVisible(false);
                // snorlax.setVisible(false);
                // pikachu.setVisible(false);
                // poochyena.setVisible(false);
                // rayquaza.setVisible(false);
                // treecko.setVisible(false);
                // eevee.setVisible(false);
                // zigzagoon.setVisible(false);

                if(trainer.getBag().get(0).id==1){
                    // show.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("mudkipuse.png"));
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==2){
                  
                    // torchic.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("torchicuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==3){
            
                    // arceus.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("arceususe.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==4){
              
                    // palkia.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("palkiause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==5){
                
                    // snorlax.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("snorlaxuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==6){
               
                    // pikachu.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("pikachuuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==7){
          
                    // poochyena.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("poochyenause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==8){
                    
                    // rayquaza.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("rayquazause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==9){

                    // treecko.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("treeckouse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==10){
                    // eevee.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("eeveeuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(0).id==11){
                    
                    // zigzagoon.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("zigzagoonuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                }
        });
        name[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                // mudkip.setVisible(false);
                // torchic.setVisible(false);
                // arceus.setVisible(false);
                // palkia.setVisible(false);
                // snorlax.setVisible(false);
                // pikachu.setVisible(false);
                // poochyena.setVisible(false);
                // rayquaza.setVisible(false);
                // treecko.setVisible(false);
                // eevee.setVisible(false);
                // zigzagoon.setVisible(false);
                if(trainer.getBag().get(1).id==1){
                    // mudkip.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("mudkipuse.png"));
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==2){
                  
                    // torchic.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("torchicuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==3){
            
                    // arceus.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("arceususe.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==4){
              
                    // palkia.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("palkiause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==5){
                
                    // snorlax.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("snorlaxuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==6){
               
                    // pikachu.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("pikachuuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==7){
          
                    // poochyena.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("poochyenause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==8){
                    
                    // rayquaza.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("rayquazause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==9){

                    // treecko.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("treeckouse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==10){
                    // eevee.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("eeveeuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(1).id==11){
                    
                    // zigzagoon.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("zigzagoonuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
            }
        });
        name[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                // mudkip.setVisible(false);
                // torchic.setVisible(false);
                // arceus.setVisible(false);
                // palkia.setVisible(false);
                // snorlax.setVisible(false);
                // pikachu.setVisible(false);
                // poochyena.setVisible(false);
                // rayquaza.setVisible(false);
                // treecko.setVisible(false);
                // eevee.setVisible(false);
                // zigzagoon.setVisible(false);
                if(trainer.getBag().get(2).id==1){
                    // mudkip.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("mudkipuse.png"));
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==2){
                  
                    // torchic.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("torchicuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==3){
            
                    // arceus.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("arceususe.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==4){
              
                    // palkia.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("palkiause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==5){
                
                    // snorlax.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("snorlaxuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==6){
               
                    // pikachu.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("pikachuuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==7){
          
                    // poochyena.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("poochyenause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==8){
                    
                    // rayquaza.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("rayquazause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==9){

                    // treecko.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("treeckouse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==10){
                    // eevee.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("eeveeuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(2).id==11){
                    
                    // zigzagoon.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("zigzagoonuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
            }
        });
        name[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                // mudkip.setVisible(false);
                // torchic.setVisible(false);
                // arceus.setVisible(false);
                // palkia.setVisible(false);
                // snorlax.setVisible(false);
                // pikachu.setVisible(false);
                // poochyena.setVisible(false);
                // rayquaza.setVisible(false);
                // treecko.setVisible(false);
                // eevee.setVisible(false);
                // zigzagoon.setVisible(false);
                if(trainer.getBag().get(3).id==1){
                    // mudkip.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("mudkipuse.png"));
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==2){
                  
                    // torchic.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("torchicuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==3){
            
                    // arceus.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("arceususe.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==4){
              
                    // palkia.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("palkiause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==5){
                
                    // snorlax.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("snorlaxuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==6){
               
                    // pikachu.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("pikachuuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==7){
          
                    // poochyena.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("poochyenause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==8){
                    
                    // rayquaza.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("rayquazause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==9){

                    // treecko.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("treeckouse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==10){
                    // eevee.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("eeveeuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(3).id==11){
                    
                    // zigzagoon.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("zigzagoonuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
            }
        });
        name[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                // mudkip.setVisible(false);
                // torchic.setVisible(false);
                // arceus.setVisible(false);
                // palkia.setVisible(false);
                // snorlax.setVisible(false);
                // pikachu.setVisible(false);
                // poochyena.setVisible(false);
                // rayquaza.setVisible(false);
                // treecko.setVisible(false);
                // eevee.setVisible(false);
                // zigzagoon.setVisible(false);
                if(trainer.getBag().get(4).id==1){
                    // mudkip.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("mudkipuse.png"));
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==2){
                  
                    // torchic.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("torchicuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==3){
            
                    // arceus.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("arceususe.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==4){
              
                    // palkia.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("palkiause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==5){
                
                    // snorlax.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("snorlaxuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==6){
               
                    // pikachu.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("pikachuuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==7){
                    // poochyena.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("poochyenause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==8){
                    
                    // rayquaza.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("rayquazause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==9){

                    // treecko.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("treeckouse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==10){
                    // eevee.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("eeveeuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(4).id==11){
                    
                    // zigzagoon.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("zigzagoonuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
            }
        });
        name[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                if(trainer.getBag().get(5).id==1){
                    // mudkip.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("mudkipuse.png"));
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==2){
                  
                    // torchic.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("torchicuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==3){
            
                    // arceus.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("arceususe.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==4){
              
                    // palkia.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("palkiause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==5){
                
                    // snorlax.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("snorlaxuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==6){
               
                    // pikachu.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("pikachuuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==7){
          
                    // poochyena.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("poochyenause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==8){
                    
                    // rayquaza.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("rayquazause.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==9){

                    // treecko.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("treeckouse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==10){
                    // eevee.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("eeveeuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
                    else if(trainer.getBag().get(5).id==11){
                    
                    // zigzagoon.setBounds(110,100,600,600);
                    show.setIcon(new ImageIcon("zigzagoonuse.png"));
                    System.out.println("Debug");
                    background.add(show);
                    }
            }
        });





















      }

}