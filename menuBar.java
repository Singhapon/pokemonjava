import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
//***MENU BAR***//
public class menuBar extends JFrame{
    private Trainer trainer;
    public menuBar(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        
        JLabel background;
        ImageIcon bg = new ImageIcon("bgmenu.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
         JButton pokemonMenu = new JButton();
         pokemonMenu.setBounds(170, 140,300,100);
         background.add(pokemonMenu);
         pokemonMenu.setIcon(new ImageIcon("pokemonmenu.png"));

         JButton bagMenu = new JButton();
         bagMenu.setBounds(760, 140,300,100);
         background.add(bagMenu);
         bagMenu.setIcon(new ImageIcon("bagmenu.png"));

         JButton fightMenu = new JButton();
         fightMenu.setBounds(170, 370,300,100);
         background.add(fightMenu);
         fightMenu.setIcon(new ImageIcon("fightmenu.png"));

         JButton statMenu = new JButton();
         statMenu.setBounds(760, 370,300,100);
         background.add(statMenu);
         statMenu.setIcon(new ImageIcon("statmenu.png"));

         JButton shopMenu = new JButton();
         shopMenu.setBounds(460, 550 , 300 , 100);
         background.add(shopMenu);
         shopMenu.setIcon(new ImageIcon("shopmenu.png"));

         JButton quit = new JButton("Quit");
         quit.setBounds(10, 10,60,50);
         background.add(quit);
         quit.setIcon(new ImageIcon("quitbar.png"));   

         pokemonMenu.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent d){
                 pokemonGui cd = new pokemonGui(trainer);
                 setVisible(false);
             }
         });

         bagMenu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent d){
                bagGui cd = new bagGui(trainer);
                setVisible(false);
            }
        });

        fightMenu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent d){
                fightGui cd = new fightGui(trainer);
                setVisible(false);
            }
        });

        statMenu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent d){
                PokemonStat cd = new PokemonStat(trainer);
                setVisible(false);
            }
        });

        quit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent d){
                System.exit(0);
            }
        });

        shopMenu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                shopgui sd = new shopgui(trainer);
                setVisible(false);
            }
        });
    }
}