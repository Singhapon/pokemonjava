import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.util.*;
public class GuiTau3 extends JFrame{
    private Trainer trainer;
    private ArrayList<Pokemon> pokemons;  
    public GuiTau3(Trainer trainer){
        this.pokemons = trainer.getBag();
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("bgmenufield.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        JLabel show = new JLabel();
        show.setBounds(110,100,600,600);
        JButton back = new JButton();
        back.setBounds(10,10,70,40);
        background.add(back);
        back.setIcon(new ImageIcon("back.png"));

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                PokemonStat mg = new PokemonStat(trainer);
                setVisible(false);
            }
        });
        if(pokemons.get(2).id==1){
            show.setIcon(new ImageIcon("mudkipuse.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==2){
            show.setIcon(new ImageIcon("torchicuse.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==3){
            show.setIcon(new ImageIcon("arceususe.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==4){
            show.setIcon(new ImageIcon("palkiause.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==5){
            show.setIcon(new ImageIcon("snorlaxuse.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==6){
            show.setIcon(new ImageIcon("pikachuuse.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==7){
            show.setIcon(new ImageIcon("poochyenause.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==8){
            show.setIcon(new ImageIcon("rayquazause.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==9){
            show.setIcon(new ImageIcon("treeckouse.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==10){
            show.setIcon(new ImageIcon("eeveeuse.png"));
            background.add(show);
        }
        else if(pokemons.get(2).id==11){
            show.setIcon(new ImageIcon("zigzagoonuse.png"));
            background.add(show);
        }
        JLabel name = new JLabel(pokemons.get(2).getName());
        name.setBounds(800,120,800,200);
        name.setFont(new Font("Hemi Head", Font.BOLD, 32));
        JLabel hp = new JLabel("HP: " + pokemons.get(2).getHp());
        hp.setBounds(800,170,800,200);
        hp.setFont(new Font("Hemi Head", Font.BOLD, 16));
        JLabel mp = new JLabel("MP: " + pokemons.get(2).getMp());
        mp.setBounds(800,220,800,200);
        mp.setFont(new Font("Hemi Head", Font.BOLD, 16));
        JLabel atk = new JLabel("ATK: " + pokemons.get(2).getAtk());
        atk.setBounds(800,270,800,200);
        atk.setFont(new Font("Hemi Head", Font.BOLD, 16));        JLabel type = new JLabel("Type : " + pokemons.get(0).gettype());
        type.setBounds(800,20,800,800);
        type.setFont(new Font("Hemi Head", Font.BOLD, 16));        JLabel def = new JLabel("DEF: " + pokemons.get(2).getDef());
        def.setBounds(800,370,800,200);
        def.setFont(new Font("Hemi Head", Font.BOLD, 16));        JLabel spatk = new JLabel("SP ATK: " + pokemons.get(2).getspAtk());
        spatk.setBounds(800,420,800,200);
        spatk.setFont(new Font("Hemi Head", Font.BOLD, 16));        JLabel spdef = new JLabel("SP DEF: " + pokemons.get(2).getspDef());
        spdef.setBounds(800,470,800,200);
        spdef.setFont(new Font("Hemi Head", Font.BOLD, 16));
        JLabel speed = new JLabel("Speed : " + pokemons.get(2).getspeed()); 
        speed.setBounds(800,520,800,200);
        speed.setFont(new Font("Hemi Head", Font.BOLD, 16));
        background.add(name);
        background.add(hp);
        background.add(mp);
        background.add(type);    
        background.add(atk);
        background.add(def);
        background.add(spatk);
        background.add(spdef);
        background.add(speed);
    }
}