import java.rmi.activation.ActivationDesc;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PokemonStat extends JFrame{
    private Trainer trainer;

    public PokemonStat(Trainer trainer){

        this.trainer=trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("statbg.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();

    JButton name[] = {new JButton(),new JButton(),new JButton(),new JButton(),new JButton(),new JButton()};
    int i=0,y=20;
    for(Pokemon p:trainer.getBag()){
        name[i].setText(" " + p.getName());
        name[i].setBounds(450 , y , 362 , 54);
        y+=100;
        background.add(name[i]);
        i++;
        }

            name[0].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    GuiTau1 mg = new GuiTau1(trainer);
                    setVisible(false);
                }
            });

            name[1].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    GuiTau2 mg = new GuiTau2(trainer);
                    setVisible(false);
                }
            });

            name[2].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    GuiTau3 mg = new GuiTau3(trainer);
                    setVisible(false);
                }
            });

            name[3].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    GuiTau4 mg = new GuiTau4(trainer);
                    setVisible(false);
                }
            });

            name[4].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    GuiTau5 mg = new GuiTau5(trainer);
                    setVisible(false);
                }
            });

            name[5].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    GuiTau6 mg = new GuiTau6(trainer);
                    setVisible(false);
                }
            });

            JButton back = new JButton();
            back.setBounds(10,10,70,40);
            background.add(back);
            back.setIcon(new ImageIcon("back.png"));


            back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                menuBar mg = new menuBar(trainer);
                setVisible(false);
            }
        });
            

        
    }
}