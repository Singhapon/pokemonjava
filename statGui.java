import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.util.*;
public class statGui extends JFrame{
    private Trainer trainer;
    private ArrayList<Pokemon> pokemons;  
    public statGui(Trainer trainer){
        this.pokemons = trainer.getBag();
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);

        JButton back = new JButton("Back");
        back.setBounds(10,10,70,40);
        c.add(back);

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                PokemonStat mg = new PokemonStat(trainer);
                setVisible(false);
            }
        });

        JLabel name = new JLabel(pokemons.get(0).getName());
        name.setBounds(800,200,80,40);
        JLabel hp = new JLabel("HP: " + pokemons.get(0).getHp());
        hp.setBounds(800,250,80,40);
        JLabel mp = new JLabel("MP: " + pokemons.get(0).getMp() + " / " + Integer.toString(pokemons.get(0).getmaxMp()));
        mp.setBounds(800,300,80,40);
        JLabel type = new JLabel(" " + pokemons.get(0).gettype());
        type.setBounds(800,350,80,40);
        JLabel atk = new JLabel("ATK: " + pokemons.get(0).getAtk());
        atk.setBounds(800,400,80,40);
        JLabel def = new JLabel("DEF: " + pokemons.get(0).getDef());
        def.setBounds(800,450,80,40);
        JLabel spatk = new JLabel("SP ATK: " + pokemons.get(0).getspAtk());
        spatk.setBounds(800,500,80,40);
        JLabel spdef = new JLabel("SP DEF: " + pokemons.get(0).getspDef());
        spdef.setBounds(800,550,80,40);
        JLabel speed = new JLabel("Speed : " + pokemons.get(0).getspeed()); 
        speed.setBounds(800,600,80,40);
        c.add(name);
        c.add(hp);
        c.add(mp);
        c.add(type);
        c.add(atk);
        c.add(def);
        c.add(spatk);
        c.add(spdef);
        c.add(speed);
        
    }
}