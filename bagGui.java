import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
public class bagGui extends JFrame{
    private Trainer trainer;
    public bagGui(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("bgmenu.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        JButton back = new JButton();
        back.setBounds(10,10,70,40);
        background.add(back);
        back.setIcon(new ImageIcon("back.png"));

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                menuBar mg = new menuBar(trainer);
                setVisible(false);
            }
        });

        JButton pokeball = new JButton();
        pokeball.setBounds(300,450,200,90);
        background.add(pokeball);
        pokeball.setIcon(new ImageIcon("pokeball.png"));

        pokeball.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                pokeballGui mg = new pokeballGui(trainer);
                setVisible(false);
            }
        });

        JButton poTion = new JButton();
        poTion.setBounds(800,450,200,90);
        background.add(poTion);
        poTion.setIcon(new ImageIcon("potion.png"));

        poTion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                potionGui mg = new potionGui(trainer);
                setVisible(false);
            }
        });
    }
}