import java.util.*;

public class PokemonRandomizer {
    public static ArrayList<Pokemon> getPokemons(int num){
        
        ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
        int pokemonNumber = (int)(Math.random() * 10);
        if(num==0){
            return pokemons;
        }
        for(int i=0; i < pokemonNumber ; ++i){
            int type = (int)(Math.random()*10);
                if(type==0)
                    pokemons.add(new Mudkip("Wild Mudkip"));
                else if(type==1)
                    pokemons.add(new Torchic("Wild Torchic"));
                else if(type==2)
                    pokemons.add(new Treecko("Wild Treecko"));
                else if(type==3)
                    pokemons.add(new Poochyena("Wild Poochyena"));
                else if(type==4)
                    pokemons.add(new Eevee("Wild Eevee"));
                else if(type==5)
                    pokemons.add(new Zigzagoon("Wild Zigzagoon"));
                else if(type==6)
                    pokemons.add(new Pikachu("Wild Pikachu"));
                else if(type==7)
                    pokemons.add(new Rayquaza("Rayquaza"));
                else if(type==8)
                    pokemons.add(new Arceus("Arceus"));
                else if(type==9)
                    pokemons.add(new Palkia("Palkia"));
                else if(type==10)
                    pokemons.add(new Snorlax("Snorlax"));
        }
        return pokemons;
    }

}