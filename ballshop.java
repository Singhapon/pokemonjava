import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
public class ballshop extends JFrame{
    private Trainer trainer;
    public ballshop(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("ballshop.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        JLabel coin = new JLabel(" X " + trainer.showMoney());
        coin.setBounds(1100,650,180,40);
        background.add(coin);
        JButton back = new JButton();
        back.setBounds(10,10,70,40);
        background.add(back);
        back.setIcon(new ImageIcon("back.png"));

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                shopgui mg = new shopgui(trainer);
                setVisible(false);
            }
        });
        JButton pokeball = new JButton("Pokeball : " + Integer.toString(trainer.getPokeball().get(0)));
        pokeball.setBounds(280, 310, 200 , 40);
        background.add(pokeball);
        pokeball.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("Money");
                if(trainer.showMoney()>=100){
                trainer.getMoney(100);
                trainer.buyball(0);
                pokeball.setText("Pokeball : " + Integer.toString(trainer.getPokeball().get(0)));
                coin.setText(" X " + trainer.showMoney());
                }

            }
        });

        JButton greatball = new JButton("Great Ball: " + Integer.toString(trainer.getPokeball().get(1)));
        greatball.setBounds(780, 300 , 200 , 40);
        background.add(greatball);
        greatball.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(trainer.showMoney()>=200){
                trainer.getMoney(200);
                trainer.buyball(1);
                greatball.setText("Great Ball: " + Integer.toString(trainer.getPokeball().get(1)));
                coin.setText(" X " + trainer.showMoney());
                }

            }
        });

        JButton ultraball = new JButton("Ultra ball: " + Integer.toString(trainer.getPokeball().get(2)));
        ultraball.setBounds(280, 580 , 200 , 40);
        background.add(ultraball);
        ultraball.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(trainer.showMoney()>=400){
                trainer.getMoney(400);
                trainer.buyball(2);
                ultraball.setText("Ultra ball: " + Integer.toString(trainer.getPokeball().get(2)));
                coin.setText(" X " + trainer.showMoney());
                }

            }
        });

        JButton masterball = new JButton("Master Ball: " + Integer.toString(trainer.getPokeball().get(3)) );
        masterball.setBounds(780, 580 , 200 , 40);
        background.add(masterball);
        masterball.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(trainer.showMoney()>=600){
                trainer.getMoney(600);
                trainer.buyball(3);
                masterball.setText("Master Ball: " + Integer.toString(trainer.getPokeball().get(3)));
                coin.setText(" X " + trainer.showMoney());
                }

            }
        });
        
    }
}