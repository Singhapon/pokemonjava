import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
public class pokeballGui extends JFrame{
    private Pokemon pokemons;
    private Trainer trainer;
    public pokeballGui(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("pokeballbag.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        

        JLabel pokeball = new JLabel("Pokeball : " + Integer.toString(trainer.getPokeball().get(0)));
        pokeball.setBounds(600, 100, 100 , 40);
        pokeball.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(pokeball);

        JLabel greatball = new JLabel("Great Ball: " + Integer.toString(trainer.getPokeball().get(1)));
        greatball.setBounds(600, 200 , 100 , 40);
        greatball.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(greatball);

        JLabel ultraball = new JLabel("Ultra ball: " + Integer.toString(trainer.getPokeball().get(2)));
        ultraball.setBounds(600, 300 , 100 , 40);
       ultraball.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(ultraball);

        JLabel masterball = new JLabel("Master Ball: " + Integer.toString(trainer.getPokeball().get(3)) );
        masterball.setBounds(600, 220 , 400 , 400);
        masterball.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(masterball);

        JButton back = new JButton();
            back.setBounds(10,10,70,40);
            background.add(back);
            back.setIcon(new ImageIcon("back.png"));

            back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                bagGui mg = new bagGui(trainer);
                setVisible(false);
            }
        });
    }
}



