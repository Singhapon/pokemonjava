public class Eevee extends Pokemon{
    public Eevee(String name){
        super(name, 200 ,50,"Water",16,17,70,100,50,10);
    }
    public void attack(Pokemon enemy){
        System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(50);
       
    }


    public String skillName1(){
        return "Tail Whip";
    }
    public String skillName2(){
        return "Quick Attack";
    }
    public String skillName3(){
        return "Trump Card";
    }

    
    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(70);
        System.out.println(name + " HP: " + hp + " use skill Tail Whip");
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(80);
        System.out.println(name + " HP: " + hp + " use skill Quick Attack");
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(140);
        System.out.println(name + " HP: " + hp + " use Trump Card" );
    }


    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Fire")){
            return damage + 20;
        }
        else if(enemy.gettype().equals("Grass")){
            return damage + 20;
        }
        else 
            return damage;
    }
}