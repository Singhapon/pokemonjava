import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;

public class ChoosePokemon extends JFrame{
    private Trainer trainer;
    public ChoosePokemon(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        // JButton ChoosePoke = new JButton("Choose Your Pokemon");
        // ChoosePoke.setBounds(500,600,200,80);
        // getContentPane();
        // c.add(ChoosePoke);
        JLabel background;
        ImageIcon bg = new ImageIcon("ChoosePokemon.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();




        JButton Mudkip = new JButton();
        Mudkip.setBounds(140,500,150,50);
        background.add(Mudkip);
        Mudkip.setIcon(new ImageIcon("choosemudkip.png"));
        
        JButton Torchic = new JButton();
        Torchic.setBounds(570,500,150,50);
        background.add(Torchic);
        Torchic.setIcon(new ImageIcon("Iconbar.png"));

        JButton Treecko = new JButton();
        Treecko.setBounds(1010,500,150,50);
        background.add(Treecko);
        Treecko.setIcon(new ImageIcon("choosetreecko.png"));

        Mudkip.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent d){
                  menuBar mg = new menuBar(trainer);
                  trainer.getBag().add(new Mudkip("Mudkip"));
                  System.out.println(trainer.getBag().get(0));
                //   trainer.potionBack();
                  setVisible(false);
              }
          });
        Torchic.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent s){
                  menuBar cd = new menuBar(trainer);
                  trainer.getBag().add(new Torchic("Torchic"));
                //   trainer.potionBack();
                  setVisible(false);
            }

        });
        Treecko.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent f){
                menuBar sm = new menuBar(trainer);
                trainer.getBag().add(new Treecko("Treecko"));
                // trainer.potionBack();
                setVisible(false);
            }
        });
    }
}
            