import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
import java.util.*;
public class fightGui extends JFrame{
    private Pokemon pk;
    private Trainer trainer;
    public fightGui(Trainer trainer){
        this.trainer = trainer;
        ArrayList<Pokemon> pokemons = PokemonRandomizer.getPokemons(11);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("bgmenu.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
    JButton fight = new JButton();
        fight.setBounds(506,233, 200 , 90);
        background.add(fight);
        fight.setIcon(new ImageIcon("fightbar.png"));

        JButton run = new JButton();
        run.setBounds(506, 433 , 200, 90);
        background.add(run);
        run.setIcon(new ImageIcon("runbar.png"));

        run.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent d){
                menuBar xd = new menuBar(trainer);
                setVisible(false);
            }
        });
        pk=pokemons.get(0);
        fight.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent s){
                battle vs = new battle(trainer,pk);
                setVisible(false);
            }
        });

        JButton back = new JButton();
        back.setBounds(10,10,70,40);
        background.add(back);
        back.setIcon(new ImageIcon("back.png"));

        back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                menuBar mg = new menuBar(trainer);
                setVisible(false);
    }
});
    
    }
}