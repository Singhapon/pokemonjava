import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
public class Trainer{
    private ArrayList<Pokemon> bag = new ArrayList<Pokemon>();  
    private ArrayList<Integer> poTionBag = new ArrayList<Integer>();
    private ArrayList<Integer> pokebag = new ArrayList<Integer>();
    private ArrayList<Pokemon> bag1 = new ArrayList<Pokemon>();
    private Scanner sc; 
    private int in ,hp,maxHp;
    private String name;
    private int smallHp = 10;
    private int mediumHp = 5;
    private String cmd = "";
    private int largeHp = 2;
    private int money = 500;
    private int mp,maxMp;
    public Trainer(String name){
        this.name = name;
        poTionBag.add(10);
        poTionBag.add(5);
        poTionBag.add(2);
        poTionBag.add(0);
        poTionBag.add(5);
        poTionBag.add(0);
        pokebag.add(10);// get(0)
        pokebag.add(5);// get(1)
        pokebag.add(2);// get(2)
        pokebag.add(0);//get(3)
         ArrayList<Pokemon> pokemons = PokemonRandomizer.getPokemons(11);
        int pokemonNumber = (int)((Math.random() * 5));//enemy
    }
    public String getName(){
        return this.name;
    }
    public int plusmp(int value){
        return mp+value;
    }

    public void printPokemon(final ArrayList<Pokemon> pokemons){
        int number = 0;
       for(final Pokemon p: pokemons){
            System.out.println("\nNo."+ number + " " +p+"\tHP:"+p.getHp()+"/"+ p.maxHp());
            number++;
        } 
    }

    public void mainName(String name){
        this.name = name;
    }
    public int money (int value){
        return money + value;
    }

    public ArrayList<Pokemon> getenemyBag(){
        return bag1;
    }

    public ArrayList<Pokemon> getBag(){
        return bag;
    }
    public void buypotion(int num){
            poTionBag.set(num,poTionBag.get(num)+1);
    }
    public void buyball(int num){
            pokebag.set(num,pokebag.get(num)+1);
    }
    public void usePotion(int num){
        if(poTionBag.get(num)>0){
            poTionBag.set(num,poTionBag.get(num)-1);
        }
    }
    public ArrayList<Integer> getPotionBag(){
        return poTionBag;
    }
    public void addMoney(int value){
        this.money=money+value;
    }
        
    public ArrayList<Integer> getPokeball(){
        return pokebag;
    }
    public int showMoney(){
        return money;
    }
    public void getMoney(int value){
        if(money-value >= 0)
            this.money = money-value;
    }
    public int showHp(){
        return hp;
    }
    public int showmaxHp(){
        return maxHp;
    }
    public int getMp(){
        return mp;
    }
    public void reducemp(int mpuse){
        this.mp = mp-mpuse;

    }
    public void useball(int num){
        if(pokebag.get(num)>0){
        pokebag.set(num,pokebag.get(num)-1);
        }
        else
        System.out.println("Out of pokeball");
    }
    public void changename(String name){
        this.name = name;
    }
}