public abstract class Pokemon {
    protected String name;

    protected int hp;
    protected int atk;
    protected int def;
    protected int maxHp;
    protected int Sp;
    protected int Spatk;
    protected int Spdef;
    protected int speed;
    protected String type;
    protected int mp;
    protected int money;
    protected int maxMp;
    protected int id;

    public Pokemon(String name,int maxHp,int atk,String type,int Spatk,int Spdef,int speed,int maxMp,int money,int id){
        this.name = name;
        this.mp = (int)((Math.random()*maxMp)+50);
         this.hp = (int)((Math.random()*maxHp)+50);
         this.maxHp = hp;
         this.type = type;
         this.atk = atk;
         this.maxMp = mp;
         this.id = id;
	    this.money = money;
         this.def = (int)((Math.random()*100)+20);
         this.Spatk = (int)((Math.random()*100)+20);
         this.Spdef = (int)((Math.random()*100)+20);
         this.Sp = Sp;
         this.speed = (int)((Math.random()*100)+20);
    }
    public int maxHp(){
        return maxHp;
    }
    public int getmaxMp(){
        return maxMp;
    }
    public void changename(String name){
        this.name = name;
    }
    public void heal(int value){
        this.hp+=value;
        if(this.hp>=maxHp){
            this.hp=maxHp;
        }
    }
    public void plusmp(int value){
        this.mp+=value;
        if(this.mp>=maxMp){
            this.mp=maxMp;
        }
    }
    public int getMp(){
        return mp;
    }
    public int getHp(){
        return hp;
    }
    public String getType(){
        return type;
    }
    public int getAtk(){
        return atk;
    } 
    public int getDef(){
        return def;
    }
    public int getspAtk(){
        return Spatk;
    }
    public int getspDef(){
        return Spdef;
    }
    public int getspeed(){
        return speed;
    }
    public boolean damage(int value){
        int currentHP = hp - value;
        if(hp == 0){
            return false;
        }
        if(currentHP >= 0)
            this.hp = currentHP;
        else
            this.hp = 0;    
        return true;   
    } 
    
    public String getName(){
        return name;
    }
    public abstract void attack(Pokemon enemy);
    public abstract void skill1(Pokemon enemy);
    public abstract void skill2(Pokemon enemy);
    public abstract void skill3(Pokemon enemy);
    public abstract String skillName1();
    public abstract String skillName2();
    public abstract String skillName3();


    public String toString(){
        return name;
    }
    public String gettype(){
        return type;
    }
    
}