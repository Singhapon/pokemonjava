public class Pikachu extends Pokemon{
    public Pikachu(String name){
        super(name, 200 ,50,"Water",16,17,60,200,120,6);
    }
    public void attack(Pokemon enemy){
        System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(70);
       
    }


    public String skillName1(){
        return "ThunerBolt";
    }
    public String skillName2(){
        return "Iron tail";
    }
    public String skillName3(){
        return "Thunder";
    }

    
    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(60);
        System.out.println(name + " HP: " + hp + " use skill ThunderBolt");
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(90);
        System.out.println(name + " HP: " + hp + " use Iron tail");
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(150);
        System.out.println(name + " HP: " + hp + " use Thunder" );
    }


    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Water")){
            return damage + 20;
        }
        else if(enemy.gettype().equals("Grass")){
            return damage - 20;
        }
        else 
            return damage;
    }
}