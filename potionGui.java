import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
import java.awt.Font;
public class potionGui extends JFrame{
    private Trainer trainer;
    public potionGui(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        JLabel background;
        ImageIcon bg = new ImageIcon("potionbag.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();

        JLabel smallPotion = new JLabel("Potion : " + Integer.toString(trainer.getPotionBag().get(0)));
        smallPotion.setBounds(600, 10, 200 , 200);
        smallPotion.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(smallPotion);

        JLabel superPotion = new JLabel("Super Potion : " + Integer.toString(trainer.getPotionBag().get(1)));
        superPotion.setBounds(600, 110 , 200 , 200);
        superPotion.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(superPotion);

        JLabel hyperPotion = new JLabel("Hyper Potion : " + Integer.toString(trainer.getPotionBag().get(2)));
        hyperPotion.setBounds(600, 210 , 200 , 200);
        hyperPotion.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(hyperPotion);

        JLabel maxPotion = new JLabel("Max Potion : " + Integer.toString(trainer.getPotionBag().get(3)));
        maxPotion.setBounds(600, 310 , 200 , 200);
        maxPotion.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(maxPotion);

        JLabel etherPotion = new JLabel("Ether Potion : " + Integer.toString(trainer.getPotionBag().get(4)));
        etherPotion.setBounds(600, 410 , 200 , 200);
        etherPotion.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(etherPotion);

        JLabel maxEther = new JLabel("Max Ether : " + Integer.toString(trainer.getPotionBag().get(5)));
        maxEther.setBounds(600, 510 , 200 , 200);
        maxEther.setFont(new Font("Hemi Head",Font.BOLD,16));
        background.add(maxEther);
        // JLabel mppotion = new JLabel("MP Potion : ");
        // mppotion.setBounds(r);

        JButton back = new JButton();
            back.setBounds(10,10,70,40);
            background.add(back);
            back.setIcon(new ImageIcon("back.png"));

            back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                bagGui mg = new bagGui(trainer);
                setVisible(false);
    }
});
    }
}