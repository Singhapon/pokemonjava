import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StartGame extends JFrame{
    private Trainer trainer;

    public StartGame(){
        super("Pokemon Game");
        trainer = new Trainer("");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);

        Container c = getContentPane();
        
        JLabel background;
        ImageIcon bg = new ImageIcon("StartGame.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        JButton start = new JButton();
        start.setBounds(530, 430, 198, 50);
        getContentPane();
        background.add(start);
        start.setIcon(new ImageIcon("StartButton.png"));
        start.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent s){
                String name = JOptionPane.showInputDialog(null,"Enter Trainer Name : ","Enter name here");   
                setVisible(false);
                trainer.mainName(name);
                ChoosePokemon ps = new ChoosePokemon(trainer);

            }
        });

        // start.setIcon(new ImageIcon("StartGame.png"));
        // background.setLayout(null);
        // c.add(background);
        // pack();
        }
    public static void main(String[] args){
        StartGame s = new StartGame();
    }
}










