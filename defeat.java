import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;

public class defeat extends JFrame{
    private Trainer trainer;
    public defeat(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("lose.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();

        JButton defeat = new JButton("Lose !");
        defeat.setBounds(530, 430, 200, 50);
        background.add(defeat);

        defeat.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                menuBar xd = new menuBar(trainer);
                setVisible(false);
            }
        });
    }
}