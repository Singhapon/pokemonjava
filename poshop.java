import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
public class poshop extends JFrame{
    private Trainer trainer;
    public poshop(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("poshop.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();

        JLabel coin = new JLabel(" X " + trainer.showMoney());
        coin.setBounds(1100,650,180,40);
        background.add(coin);

        JButton smallPotion = new JButton("Potion : " + Integer.toString(trainer.getPotionBag().get(0)));
        smallPotion.setBounds(250, 260, 200 , 40);
        background.add(smallPotion);
        smallPotion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent c){
                if(trainer.showMoney()>= 50){
                    trainer.getMoney(50);
                    trainer.buypotion(0);
                    smallPotion.setText("Potion : " + Integer.toString(trainer.getPotionBag().get(0)));
                    coin.setText(" X " + trainer.showMoney());

                }
            }
        });

        JButton superPotion = new JButton("Super Potion : " + Integer.toString(trainer.getPotionBag().get(1)));
        superPotion.setBounds(530, 260 , 200 , 40);
        background.add(superPotion);
        superPotion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent c){
                if(trainer.showMoney()>= 100){
                    trainer.getMoney(100);
                    trainer.buypotion(1);
                    superPotion.setText("Super Potion : " + Integer.toString(trainer.getPotionBag().get(1)));
                    coin.setText(" X " + trainer.showMoney());

                }
            }
        });
        

        JButton hyperPotion = new JButton("Hyper Potion : " + Integer.toString(trainer.getPotionBag().get(2)));
        hyperPotion.setBounds(250, 500 , 200 , 40);
        background.add(hyperPotion);
        hyperPotion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent c){
                if(trainer.showMoney()>= 200){
                    trainer.getMoney(200);
                    trainer.buypotion(2);
                    hyperPotion.setText("Hyper Potion : " + Integer.toString(trainer.getPotionBag().get(2)));
                    coin.setText(" X " + trainer.showMoney());

                }
            }
        });
        

        JButton maxPotion = new JButton("Max Potion : " + Integer.toString(trainer.getPotionBag().get(3)));
        maxPotion.setBounds(530, 500 , 200 , 40);
        background.add(maxPotion);
        maxPotion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent c){
                if(trainer.showMoney()>= 400){
                    trainer.getMoney(400);
                    trainer.buypotion(3);
                    maxPotion.setText("Max Potion : " + Integer.toString(trainer.getPotionBag().get(3)));
                    coin.setText(" X " + trainer.showMoney());

                }
            }
        });

        JButton etherPotion = new JButton("Ether Potion : " + Integer.toString(trainer.getPotionBag().get(4)));
        etherPotion.setBounds(830, 260 , 200 , 40);
        background.add(etherPotion);
        etherPotion.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent c){
                if(trainer.showMoney()>= 40){
                    trainer.getMoney(40);
                    trainer.buypotion(4);
                    etherPotion.setText("Ether Potion : " + Integer.toString(trainer.getPotionBag().get(4)));
                    coin.setText(" X " + trainer.showMoney());

                }
            }
        });

        JButton maxEther = new JButton("Max Ether : " + Integer.toString(trainer.getPotionBag().get(5)));
        maxEther.setBounds(830, 500 , 200 , 40);
        background.add(maxEther);
        maxEther.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent c){
                if(trainer.showMoney()>= 100){
                    trainer.getMoney(100);
                    trainer.buypotion(5);
                    maxEther.setText("Max Ether : " + Integer.toString(trainer.getPotionBag().get(5)));
                    coin.setText(" X " + trainer.showMoney());

                }
            }
        });

        JButton back = new JButton();
            back.setBounds(10,10,70,40);
            background.add(back);
            back.setIcon(new ImageIcon("back.png"));
            back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                shopgui mg = new shopgui(trainer);
                setVisible(false);
    }
});

    }
}
