public class Treecko extends Pokemon{
    public Treecko(String name){
        super(name, 220 ,40,"Grass",19,20,80,500,100,9);
    }
    public void attack(Pokemon enemy){
        System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(40);
       
    }
    public String skillName1(){
        return "Absorb";
    }
    public String skillName2(){
        return "Mega Drain";
    }
    public String skillName3(){
        return "Giga Drain";
    }
    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(80);
        System.out.println(name + " HP: " + hp + " use skill Absorb" );
        heal(30);
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(100);
        System.out.println(name + " HP: " + hp + " use skill Mega Drain" );
        heal(60);
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(160);
        System.out.println(name + " HP: " + hp + " use Giga Drain" );
        heal(90);

    }
    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Water")){
            return damage + 20;
        }
        else if(enemy.gettype().equals("Fire")){
            return damage - 20;
        }
        else 
            return damage;
    }
}