public class Zigzagoon extends Pokemon{
    public Zigzagoon(String name){
        super(name, 120 ,30,"Normal",10,10,30,60,40,11);
    }
    public void attack(Pokemon enemy){
        // System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(30);
       
    }

    public String skillName1(){
        return "Tackle";
    }
    public String skillName2(){
        return "Tackle";
    }
    public String skillName3(){
        return "Tackle";
    }
    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(80);
        System.out.println(name + " HP: " + hp + " use skill Ember" );
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(100);
        System.out.println(name + " HP: " + hp + " use skill Flame Charge" );
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(120);
        System.out.println(name + " HP: " + hp + " use skill Flamethrower" );
    }

    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Fire")){
            return damage + 1;
        }
        else if(enemy.gettype().equals("Water")){
            return damage + 3;
        }
        else 
            return damage + 6;
        }
    }