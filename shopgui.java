import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
public class shopgui extends JFrame{
    private Trainer trainer;
    public shopgui(Trainer trainer){
        this.trainer = trainer;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("shop.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();

        JButton back = new JButton();
            back.setBounds(10,10,70,40);
            background.add(back);
            back.setIcon(new ImageIcon("back.png"));

            back.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                menuBar mg = new menuBar(trainer);
                setVisible(false);
            }
        });
        
        JButton pokeballshop = new JButton();
        pokeballshop.setBounds(300,450,200,90);
        background.add(pokeballshop);
        pokeballshop.setIcon(new ImageIcon("shoppokeball.png"));

        pokeballshop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                ballshop sp = new ballshop(trainer);
                setVisible(false);
            }
        });

        JButton potionshop = new JButton();
        potionshop.setBounds(800,450,200,90);
        background.add(potionshop);
        potionshop.setIcon(new ImageIcon("shoppotion.png"));
        potionshop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent z){
                poshop sh = new poshop(trainer);
                setVisible(false);
            }
        });


    }
}