public class Poochyena extends Pokemon{
    public Poochyena(String name){
        super(name, 140 ,25,"Normal",30,30,90,60,60,7);
    }
    public void attack(Pokemon enemy){
        System.out.println( name + " Attack " + enemy.getName());
        enemy.damage(25);
       
    }
    public String skillName1(){
        return "Tackle";
    }
    public String skillName2(){
        return "Tackle";
    }
    public String skillName3(){
        return "Tackle";
    }
    public void skill1(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(40);
        System.out.println(name + " HP: " + hp + " use skill Tackle" );
    }
    public void skill2(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(50);
        System.out.println(name + " HP: " + hp + " use skill Tackle" );
    }
    public void skill3(Pokemon enemy){
        System.out.println("\n" + name + " attack " + enemy.getName());
        enemy.damage(60);
        System.out.println(name + " HP: " + hp + " use skill Tackle" );
    }

    public int specialDamage(Pokemon enemy,int damage){
        if(enemy.gettype().equals("Fire")){
            return damage + 8;
        }
        else if(enemy.gettype().equals("Water")){
            return damage + 5;
        }
        else 
            return damage + 2;
        }
    }   