import java.rmi.activation.ActivationDesc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JButton;
import java.util.*;
public class battle extends JFrame{
    private Trainer trainer;
    private Pokemon pokemon;
    private ArrayList<Pokemon> enemy;
    // private ArrayList<Pokemon> pokemons; 
    public battle(Trainer trainer,Pokemon pokemons){
        
        // enemy = new ArrayList<Pokemon>();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1270, 720);
        setVisible(true);
        Container c = getContentPane();
        c.setLayout(null);
        JLabel background;
        ImageIcon bg = new ImageIcon("field.png");
        c.setLayout(new FlowLayout());
        background = new JLabel(bg);
        background.setLayout(null);
        c.add(background);
        pack();
        if(trainer.getBag().get(0).id==1){
            JLabel mymudkip = new JLabel();
            mymudkip.setBounds(130,190 , 500 ,400);
            mymudkip.setIcon(new ImageIcon("mymudkip.png"));
            background.add(mymudkip);
        }
        else if(trainer.getBag().get(0).id==2){
            JLabel mytorchic = new JLabel();
            mytorchic.setBounds(130,190,500,400);
            mytorchic.setIcon(new ImageIcon("mytorchic.png"));
            background.add(mytorchic);
        }
        else if(trainer.getBag().get(0).id==9){
            JLabel mytreecko = new JLabel();
            mytreecko.setBounds(130,190,500,400);
            mytreecko.setIcon(new ImageIcon("mytreecko.png"));
            background.add(mytreecko);
        }
        if(pokemons.id==1){
        JLabel mudkip = new JLabel();
        mudkip.setBounds(800,100,320,250);
        mudkip.setIcon(new ImageIcon("mudkip.png"));
        System.out.println("Debug");
        background.add(mudkip);
        }
        else if(pokemons.id==2){
        JLabel torchic = new JLabel();
        torchic.setBounds(800,100,320,250);
        torchic.setIcon(new ImageIcon("torchic.png"));
        System.out.println("Debug");
        background.add(torchic);
        }
        else if(pokemons.id==3){
        JLabel arceus = new JLabel();
        arceus.setBounds(800,100,320,250);
        arceus.setIcon(new ImageIcon("arceus.png"));
        System.out.println("Debug");
        background.add(arceus);
        }
        else if(pokemons.id==4){
        JLabel palkia = new JLabel();
        palkia.setBounds(800,100,320,250);
        palkia.setIcon(new ImageIcon("palkia.png"));
        System.out.println("Debug");
        background.add(palkia);
        }
        else if(pokemons.id==5){
        JLabel snorlax = new JLabel();
        snorlax.setBounds(800,100,320,250);
        snorlax.setIcon(new ImageIcon("snorlax.png"));
        System.out.println("Debug");
        background.add(snorlax);
        }
        else if(pokemons.id==6){
        JLabel pikachu = new JLabel();
        pikachu.setBounds(800,100,320,250);
        pikachu.setIcon(new ImageIcon("pikachu.png"));
        System.out.println("Debug");
        background.add(pikachu);
        }
        else if(pokemons.id==7){
        JLabel poochyena = new JLabel();
        poochyena.setBounds(800,100,320,250);
        poochyena.setIcon(new ImageIcon("poochyena.png"));
        System.out.println("Debug");
        background.add(poochyena);
        }
        else if(pokemons.id==8){
        JLabel rayquaza = new JLabel();
        rayquaza.setBounds(800,100,320,250);
        rayquaza.setIcon(new ImageIcon("rayquaza.png"));
        System.out.println("Debug");
        background.add(rayquaza);
        }
        else if(pokemons.id==9){
        JLabel treecko = new JLabel();
        treecko.setBounds(800,100,320,250);
        treecko.setIcon(new ImageIcon("treecko.png"));
        System.out.println("Debug");
        background.add(treecko);
        }
        else if(pokemons.id==10){
        JLabel eevee = new JLabel();
        eevee.setBounds(800,100,320,250);
        eevee.setIcon(new ImageIcon("eevee.png"));
        System.out.println("Debug");
        background.add(eevee);
        }
        else if(pokemons.id==11){
        JLabel zigzagoon = new JLabel();
        zigzagoon.setBounds(800,100,320,250);
        zigzagoon.setIcon(new ImageIcon("zigzagoon.png"));
        System.out.println("Debug");
        background.add(zigzagoon);
        }

    
    //BATTLE!
    JLabel enemyhp = new JLabel(" " + pokemons.getName() + " : " + pokemons.getHp() + " / " + pokemons.maxHp());
     enemyhp.setBounds(200,10,200,200);
     System.out.println(" " + pokemons.getName());
     enemyhp.setFont(new Font("Courier", Font.BOLD,16));
     background.add(enemyhp);

    JButton fightPoke = new JButton(" Tackle ");
    fightPoke.setBounds(860,550,170,60);
    background.add(fightPoke);

    JButton catchPoke = new JButton(" " + trainer.getBag().get(0).skillName1());
    catchPoke.setBounds(1020, 550 , 170 , 60);
    background.add(catchPoke);
    // catchPoke.addActionListener(new ActionListener(){

    // }

    JButton itemPoke = new JButton(" " + trainer.getBag().get(0).skillName2());
    itemPoke.setBounds(860,630, 170 , 60);
    background.add(itemPoke);

    JButton runPoke = new JButton(" " + trainer.getBag().get(0).skillName3());
    runPoke.setBounds(1020,630, 170, 60);
    background.add(runPoke);
    
    JLabel hp = new JLabel(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp() );
    JLabel mp = new JLabel("MP : " + trainer.getBag().get(0).getMp() + " / " + trainer.getBag().get(0).getmaxMp());
    hp.setFont(new Font("Courier", Font.BOLD,20));
    mp.setFont(new Font("Courier", Font.BOLD,20));
    mp.setBounds(800, 400,800,80);
    hp.setBounds(800, 370, 800, 80);
    background.add(hp);
    background.add(mp);

    fightPoke.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent s){
            if(trainer.getBag().get(0).getHp() != 0 && pokemons.getHp() != 0 && trainer.getBag().get(0).getMp()> 40){
                trainer.getBag().get(0).attack(pokemons);
                trainer.reducemp(40);
                pokemons.attack(trainer.getBag().get(0));
                enemyhp.setText(" " + pokemons.getName() + " : " + pokemons.getHp() + " / " + pokemons.maxHp());
                mp.setText("MP : " + trainer.getBag().get(0).getMp() + " / " + trainer.getBag().get(0).getmaxMp());
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                if(trainer.getBag().get(0).getHp() <= 0){
                    defeat df = new defeat(trainer);
                    setVisible(false);
                }
                else if(pokemons.getHp() <= 0){
                    trainer.addMoney(pokemons.money);
                    win wi = new win(trainer);
                    setVisible(false);
                  
                }
            }
    }
    });


    catchPoke.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent a){
            if(trainer.getBag().get(0).getHp() != 0 || pokemons.getHp() != 0 && trainer.getBag().get(0).getMp() > 50){
                trainer.getBag().get(0).skill1(pokemons);
                trainer.reducemp(60);
                System.out.println(" " + trainer.getBag().get(0).getHp());
                if(pokemons.getHp() != 0 && trainer.getBag().get(0).getHp() != 0)
                pokemons.attack(trainer.getBag().get(0));
                enemyhp.setText(" " + pokemons.getName() + " : " + pokemons.getHp() + " / " + pokemons.maxHp());
                mp.setText("MP : " + trainer.getBag().get(0).getMp() + " / " + trainer.getBag().get(0).getmaxMp());
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                if(trainer.getBag().get(0).getHp() <= 0){
                    defeat df = new defeat(trainer);
                    setVisible(false);
                }
                else if(pokemons.getHp() <= 0){
                    trainer.addMoney(pokemons.money);
                    win wi = new win(trainer);
                    setVisible(false);
                }
            }
        }
    });

    itemPoke.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent j){
            if(trainer.getBag().get(0).getHp() != 0 || pokemons.getHp() != 0 && trainer.getBag().get(0).getMp() > 60){
                trainer.getBag().get(0).skill2(pokemons);
                trainer.reducemp(80);
                if(pokemons.getHp() != 0 && trainer.getBag().get(0).getHp() != 0)
                pokemons.attack(trainer.getBag().get(0));
                enemyhp.setText(" " + pokemons.getName() + " : " + pokemons.getHp() + " / " + pokemons.maxHp());
                mp.setText("MP : " + trainer.getBag().get(0).getMp() + " / " + trainer.getBag().get(0).getmaxMp());
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                if(trainer.getBag().get(0).getHp() <= 0){
                    defeat df = new defeat(trainer);
                    setVisible(false);
                }
                else if(pokemons.getHp() <= 0){
                    trainer.addMoney(pokemons.money);
                    win wi = new win(trainer);
                    setVisible(false);
               
                }
            }
        }
    });

    runPoke.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent x){
            if(trainer.getBag().get(0).getHp() != 0 || pokemons.getHp() != 0 && trainer.getBag().get(0).getMp() > 70){
                trainer.getBag().get(0).skill3(pokemons);
                trainer.reducemp(100);
                if(pokemons.getHp() != 0 && trainer.getBag().get(0).getHp() != 0)
                pokemons.attack(trainer.getBag().get(0));
                enemyhp.setText(" " + pokemons.getName() + " : " + pokemons.getHp() + " / " + pokemons.maxHp());
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                mp.setText("MP : " + trainer.getBag().get(0).getMp() + " / " + Integer.toString(trainer.getBag().get(0).getmaxMp()));
                if(trainer.getBag().get(0).getHp() <= 0){
                    defeat df = new defeat(trainer);
                    setVisible(false);
                }
                else if(pokemons.getHp() <= 0){
                    trainer.addMoney(pokemons.money);
                    win wi = new win(trainer);
                    setVisible(false);
                   
                }
            }
        }
    });
    /////////////////////////////////////////////////////////////////////////////


    JButton runfight = new JButton();
    runfight.setBounds(690,600,90,60);
    background.add(runfight);
    runfight.setIcon(new ImageIcon("runfight.png"));

    runfight.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent d){
            menuBar xd = new menuBar(trainer);
            setVisible(false);
        }
    });
        
    

    JLabel catchPo = new JLabel("Catch !!");
    catchPo.setBounds(350, 525 , 60 ,40);
    background.add(catchPo);

    JButton pokeball1 = new JButton("Pokeball : " + trainer.getPokeball().get(0));
    pokeball1.setBounds(60,560,120,40);
    background.add(pokeball1);  
    pokeball1.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent l){
            if(trainer.getPokeball().get(0)!=0){
            int j;
            j=(int)(Math.random()*10);
            if(j<3){
                trainer.getBag().add(pokemons);
                catched xd = new catched(trainer);
                setVisible(false);
            }
            trainer.useball(0);
            System.out.println(" Use ball");
            pokeball1.setText("Pokeball : " + trainer.getPokeball().get(0));
        }
    }
    });
    //great ball
    JButton pokeball2 = new JButton("Great Ball : " + trainer.getPokeball().get(1));
    pokeball2.setBounds(220,560,120,40);
    background.add(pokeball2);  
    pokeball2.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent a){
            if(trainer.getPokeball().get(1)!=0){
            int j;
            j=(int)(Math.random() * 10);
            if(j<4){
                trainer.getBag().add(pokemons);
                catched xd = new catched(trainer);
                setVisible(false);
            }
            trainer.useball(1);
            pokeball2.setText("Great Ball : " + trainer.getPokeball().get(1));
        }
        }
    });
    //ultra ball

    JButton pokeball3 = new JButton("Ultra Ball : " + trainer.getPokeball().get(2));
    pokeball3.setBounds(380,560,120,40);
    background.add(pokeball3);  
    pokeball3.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent p){
            if(trainer.getPokeball().get(2)!=0){
            int j;
            j=(int)(Math.random()* 10);
            if(j<6){
                trainer.getBag().add(pokemons);
                catched xd = new catched(trainer);
                setVisible(false);
            }
            trainer.useball(2);
            pokeball3.setText("Ultra Ball : " + trainer.getPokeball().get(2));
        }
    }
    });
    //master ball
    JButton pokeball4 = new JButton("Master Ball : " + trainer.getPokeball().get(3));
    pokeball4.setBounds(540,560,120,40);
    background.add(pokeball4); 
    pokeball4.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent j){
            if(trainer.getPokeball().get(3)!=0){
                trainer.getBag().add(pokemons);
                catched xd = new catched(trainer);
                setVisible(false);
            trainer.useball(3);
            }
            // pokeball4.setText("Master Ball : " + trainer.getPokeball().get(3));
        }
    });


    JLabel potion = new JLabel("Potion");
    potion.setBounds(350,590,60,40);
    background.add(potion);

    //Potion
    JButton potion1 = new JButton("Potion : " + trainer.getPotionBag().get(0));
    potion1.setBounds(140 , 630 , 130 , 30);
    background.add(potion1);
    potion1.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent q){
            if(trainer.getPotionBag().get(0)!=0 && trainer.getBag().get(0).getHp()!=trainer.getBag().get(0).maxHp()){
                trainer.getBag().get(0).heal(40);
                trainer.usePotion(0);
                potion1.setText("Potion : " + trainer.getPotionBag().get(0));
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                
            }
        }
    });


    JButton potion2 = new JButton("Super Potion : " + trainer.getPotionBag().get(1));
    potion2.setBounds(290, 630 , 130 , 30);
    background.add(potion2);
    potion2.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            if(trainer.getPotionBag().get(1)!=0 && trainer.getBag().get(0).getHp()!=trainer.getBag().get(0).maxHp()){
                trainer.getBag().get(0).heal(80);
                trainer.usePotion(1);
                potion2.setText("Super Potion : " + trainer.getPotionBag().get(1));
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                
            }
        }
    });

    JButton potion3 = new JButton("Hyper Potion : " + trainer.getPotionBag().get(2));
    potion3.setBounds(440 , 630 , 130 , 30);
    background.add(potion3);
    potion3.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent r){
            if(trainer.getPotionBag().get(2)!=0 && trainer.getBag().get(0).getHp()!=trainer.getBag().get(0).maxHp()){
                trainer.getBag().get(0).heal(120);
                trainer.usePotion(2);
                potion3.setText("Hyper Potion : " + trainer.getPotionBag().get(2));
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                
            }
        }
    });


    JButton potion4 = new JButton("Max Potion : " + trainer.getPotionBag().get(3));
    potion4.setBounds(140 , 665 , 130 ,30);
    background.add(potion4);
    potion4.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent t){
            if(trainer.getPotionBag().get(3)!=0 && trainer.getBag().get(0).getHp()!=trainer.getBag().get(0).maxHp()){
                trainer.getBag().get(0).heal(5000);
                trainer.usePotion(3);
                potion4.setText("Max Potion : " + trainer.getPotionBag().get(3));
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                
            }
        }
    });



    JButton potion5 = new JButton("Ether : " + trainer.getPotionBag().get(4));
    potion5.setBounds(290, 665 , 130, 30);
    background.add(potion5);
    potion5.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent t){
            if(trainer.getPotionBag().get(4)!=0){
                // trainer.getBag().get(0).heal(5000);
                trainer.getBag().get(0).plusmp(50);
                trainer.usePotion(4);
                potion5.setText("Ether : " + trainer.getPotionBag().get(4));
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                
            }
        }
    });


    JButton potion6 = new JButton("Max Ether : " + trainer.getPotionBag().get(5));
    potion6.setBounds(440, 665 , 130 , 30);
    background.add(potion6);
    potion6.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent t){
            if(trainer.getPotionBag().get(5)!=0){
                trainer.getBag().get(0).plusmp(50000);
                // trainer.getBag().get(0).heal(5000);
                trainer.usePotion(5);
                potion6.setText("Max Ether : " + trainer.getPotionBag().get(5));
                hp.setText(" " + trainer.getBag().get(0).getName() + " : " + trainer.getBag().get(0).getHp() + " / " + trainer.getBag().get(0).maxHp());
                
            }
        }
    });   
    

     



    // //Action
    }
}